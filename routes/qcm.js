const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const crossOrigin = require("./../services/cross-origin");
var qcmHelper = require("./../helpers/qcm");
const logger = require("../services/logger");
var security = require("./../services/security");

// parse requests of content-type: application/json
router.use(bodyParser.json());
// parse requests of content-type: application/x-www-form-urlencoded
router.use(bodyParser.urlencoded({ extended: true }));
router.use(crossOrigin);

//////////////////////////////////////////////////////// =>

router.get("/", (req, res) => {
  res.send("qcm");
});

router.get("/get_all?", security.isAuth, (req, res) => {
  var ca;
  if (req.query.token) {
    ca = security.parseJwt(req.query.token);
  }
  console.log(ca)
  qcmHelper
    .getAllQcm()
    .then((result) => {
      res.json(result);
    })
    .catch((err) => console.log(err));
});

router.get("/get_qcm?", security.isAuth, (req, res) => {
  let id = req.query.id;
  console.log(base64Url);
  qcmHelper
    .getAllQcm(id)
    .then((result) => res.json(result))
    .catch((err) => console.log(err));
});

router.get("/get_qcm?", security.isAuth, (req, res) => {
  let id = req.query.id;
  qcmHelper
    .getQcmById(id)
    .then((result) => res.json(result))
    .catch((err) => console.log(err));
});

router.post("/post_qcm", security.isAuth, (req, res) => {
  let qcm = {
    qcm: req.body.qcm,
    createAt: req.body.createAt,
    randomPos: req.body.randomPos,
  };
  qcmHelper
    .insertQcm(qcm)
    .then((result) => {
      logger.info({ result: "qcm pushed" });
      res.json(result);
    })
    .catch((err) => {
      logger.error({ err });
      res.send(err);
    });
});

module.exports = router;
