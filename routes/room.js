const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const crossOrigin = require("./../services/cross-origin");
var roomHelper = require("./../helpers/room");
const logger = require("../services/logger");
var security = require("./../services/security");

// parse requests of content-type: application/json
router.use(bodyParser.json());
// parse requests of content-type: application/x-www-form-urlencoded
router.use(bodyParser.urlencoded({ extended: true }));
router.use(crossOrigin);

//////////////////////////////////////////////////////// =>

router.get("/", (req, res) => {
  res.send("room");
});

router.get("/get_roomId?", security.isAuth, (req, res) => {
  let id = req.query.id;
  roomHelper
    .getRoomByCode(id)
    .then((result) => res.json(result))
    .catch((err) => console.log(err));
});

router.get("/get_all", security.isAuth, (req, res) => {
  let info,
    params = "";
  if (req.query.token) {
    info = security.parseJwt(req.query.token);
    params = { users: { $all: [info._id] } };
  }
  console.log(info)
  roomHelper
    .getAllRoom(params === "" ? "" : params)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => console.log(err));
});

router.get("/get_roomCode?", security.isAuth, (req, res) => {
  let id = req.query.code;
  roomHelper
    .getRoomByCode(id)
    .then((result) => res.json(result))
    .catch((err) => console.log(err));
});

router.post("/post_room", security.isAuth, (req, res) => {
  let room = {
    room: {
      adminId: req.body.adminId,
      users: [req.body.adminId],
      qcmId: req.body.qcm,
      code: req.body.code,
      state: "closed",
    },
  };
  roomHelper
    .insertRoom(room)
    .then((result) => {
      logger.info({ result: "Room pushed" });
      res.json(result);
    })
    .catch((err) => {
      logger.error({ err });
      res.send(err);
    });
});

router.post("/post_userAtRoom", security.isAuth, (req, res) => {
  const userId = req.body.userId;
  const roomCode = req.body.roomCode;
  roomHelper
    .getRoomByCode(roomCode)
    .then(() => {
      logger.info({ message: "Room found" });
      roomHelper
        .insertUserAtRoom(userId, roomCode)
        .then(() => res.send("User pushed"));
    })
    .catch((err) => console.log(err));
});

router.post("/start", security.isAuth, (req, res) => {
  const roomId = req.body.room;
  const state = req.body.state;
  if (state === "opened") {
    roomHelper
      .getRoomById(roomId)
      .then(() => {
        roomHelper
          .updateState(roomId, state)
          .then(() => {
            logger.info({
              message: "Room state as updated",
              roomId: roomId,
              state: state,
            });
            res.status(200).send("State updated");
          })
          .catch((err) => res.send(err));
      })
      .catch((err) => res.send(err));
  } else {
    logger.error({ message: "Bad state", state: state });
    res.send("Bad state");
  }
});

router.post("/close", security.isAuth, (req, res) => {
  const roomId = req.body.room;
  const state = req.body.state;
  if (state === "closed") {
    roomHelper
      .getRoomById(roomId)
      .then(() => {
        roomHelper
          .updateState(roomId, state)
          .then(() => {
            logger.info({
              message: "Room state as updated",
              roomId: roomId,
              state: state,
            });
            res.status(200).send("State updated");
          })
          .catch((err) => res.send(err));
      })
      .catch((err) => res.send(err));
  } else {
    logger.error({ message: "Bad state", state: state });
    res.send("Bad state");
  }
});

router.post("/get_result", security.isAuth, (req, res) => {});

module.exports = router;
