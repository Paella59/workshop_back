const express = require('express');
const router = express.Router();

router.use( '/user' , require('./user.js'));
router.use( '/qcm' , require('./qcm.js'));
router.use( '/room' , require('./room.js'));

router.get('/', (req,res)=> {
	res.send('coucou c\'est l\'api =)');
});
 
module.exports = router;


