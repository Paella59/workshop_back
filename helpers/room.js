var database = require("../services/database");
const { ObjectId } = require("mongodb");
module.exports = {
  getRoomById,
  getAllRoom,
  getRoomByCode,
  insertRoom,
  insertUserAtRoom,
  updateState,
};

async function getAllRoom(params) {
  return new Promise((resolve, reject) => {
    database.injectDB((db) => {
      return db
        .collection("room")
        .find(params)
        .toArray()
        .then((room) => {
          resolve(room);
        })
        .catch((err) => reject(err));
    });
  });
}

async function getRoomById(id) {
  return new Promise((resolve, reject) => {
    if (id && typeof id == "string") {
      database.injectDB((db) => {
        return db
          .collection("room")
          .find({ _id: ObjectId(id) })
          .limit(1)
          .next()
          .then((room) => {
            if (room) {
              resolve(room);
            } else {
              reject();
            }
          });
      });
    } else {
      reject("GetRoomById: Bad id (" + typeof id + "): " + id);
    }
  });
}

async function getRoomByCode(code) {
  return new Promise((resolve, reject) => {
    if (code && typeof code == "string") {
      database.injectDB((db) => {
        return db
          .collection("room")
          .find({ code: code })
          .limit(1)
          .next()
          .then((room) => {
            if (room) {
              resolve(room);
            } else {
              reject("GetRoomByCode: Room doesn't exist");
            }
          });
      });
    } else {
      reject("GetRoomByCode: Bad Code (" + typeof code + "): " + code);
    }
  });
}

async function insertRoom(room) {
  return new Promise((resolve, reject) => {
    if (room) {
      database.injectDB((db) => {
        return db
          .collection("room")
          .insertOne(room)
          .then((result) => {
            resolve(result);
          });
      });
    } else {
      reject("insertRoom: Bad room");
    }
  });
}

async function insertUserAtRoom(user, room) {
  return new Promise((resolve, reject) => {
    if (room && user) {
      database.injectDB((db) => {
        return db
          .collection("room")
          .updateOne(
            { code: room }, // Filter
            { $push: { users: user } } // Update
          )
          .then((result) => {
            resolve(result);
          });
      });
    } else {
      reject("insertUserAtRoom: Bad room or user arleady in this");
    }
  });
}

async function updateState(room, newState) {
  return new Promise((resolve, reject) => {
    if (room && newState) {
      database.injectDB((db) => {
        return db
          .collection("room")
          .updateOne({ _id: ObjectId(room) }, { $set: { state: newState } })
          .then((result) => resolve(result))
          .catch((err) => reject(err));
      });
    } else {
      reject("updateState: bad state");
    }
  });
}
