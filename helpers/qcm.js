var database = require("../services/database");
const { ObjectId } = require("mongodb");
module.exports = {
  getAllQcm,
  getQcmById,
  getQcmByCode,
  insertQcm,
};

async function getAllQcm(params) {
  return new Promise((resolve, reject) => {
    database.injectDB((db) => {
      return db
        .collection("qcm")
        .find(params)
        .toArray()
        .then((result) => {
          resolve(result);
        })
        .catch((err) => reject(err));
    });
  });
}

async function getQcmById(id) {
  return new Promise((resolve, reject) => {
    if (id && typeof id == "string") {
      database.injectDB((db) => {
        return db
          .collection("qcm")
          .find({ _id: ObjectId(id) })
          .limit(1)
          .next()
          .then((qcm) => {
            if (qcm) {
              resolve(qcm);
            } else {
              reject();
            }
          });
      });
    } else {
      reject("GetQcmById: Bad id (" + typeof id + "): " + id);
    }
  });
}

async function getQcmByCode(code) {
    return new Promise((resolve, reject) => {
      if (code && typeof code == "string") {
        database.injectDB((db) => {
          return db
            .collection("qcm")
            .find({ code : code})
            .limit(1)
            .next()
            .then((qcm) => {
              if (qcm) {
                resolve(qcm);
              } else {
                reject();
              }
            });
        });
      } else {
        reject("GetQcmrById: Bad id (" + typeof code + "): " + code);
      }
    });
  }

async function insertQcm(qcm) {
  return new Promise((resolve, reject) => {
    if (qcm) {
      database.injectDB((db) => {
        return db
          .collection("qcm")
          .insertOne(qcm)
          .then((result) => {
            resolve(result);
          });
      });
    } else {
      reject("insertQcm: Bad qcm");
    }
  });
}
