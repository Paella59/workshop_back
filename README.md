<h1 align="center">Welcome to WorkshopBack👋</h1>
<p>
Description : 
</p>

### 🏠 [Homepage](void) LINK a mettre

### ✨ [Demo](no)

## Install

```sh
npm install 
```

## Usage

```sh
npm run start
```

## Author

👤 **Efficom Groupe 2 WK 2021**

* Github: [@toinnoux](https://github.com/synnksou) [@BabyReggae](https://github.com/BabyReggae) 
* GitLab: [@toinouux](https://gitlab.com/toinouux) [@dockguillaume](https://gitlab.com/dockguillaume)
* LinkedIn: [@Heinrich Antoine](https://fr.linkedin.com/in/antoine-heinrich-58a35016b) [@Dockwiller Guillaulme](https://fr.linkedin.com/in/guillaume-dockwiller-47050b14a)
 

