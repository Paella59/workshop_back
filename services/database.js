const mongo = require("mongodb").MongoClient;
const config = require("../config/config");
const logger = require("./logger");

module.exports = {
  injectDB,
};

function injectDB(promiseCallback) {
  return getDatabase(config.database).then((db) =>
    promiseCallback(db)
      .then((result) => {
        db.close();
        return Promise.resolve(result);
      })
      .catch((err) => {
        db.close();
        return Promise.reject(err);
      })
  );
}

function getDatabase(database) {
  let credential = "";
  if (database.user && database.password) {
    credential = `${database.user}:${database.password}@`;
  }
  const dbUri = `mongodb://${credential}${database.host}:${database.port}/${database.name}`;
  return mongo
    .connect(dbUri)
    .catch((err) =>
      logger.error({ message: "Can't connect to Mongo: " + err + "\n" + dbUri })
    );
}
